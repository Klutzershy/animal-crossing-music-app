# Animal Crossing Music App

This is a clock program which plays hourly music from the Animal Crossing games of your choice!

![alt tag](https://i.imgur.com/Kxt6q6S.png)

### Inspiration
This was inspired by the [Animal Crossing Music Extension] and by https://tane.us/ac/

## Features
* Choose which game music from GameCube, City Folk/Wild World, and New Leaf
* Volume Slider to control the volume
* Config file is saved in the same directory as the jar file. This will save the settings to load next time you open the app.

## Future Enhancements
1. K.K. Slider music at 8pm on Saturdays
2. Event music during event times
3. Town Chimes
4. Rain/Snow music according to weather
5. Improved graphics

## Download
Currently I have the download to the jar file on [Google Drive], but if I find a better place to host it I will change it.

# Legal

## Disclaimer
Copyright Disclaimer under section 107 of the Copyright Act of 1976, allowance is made for “fair use” for purposes such as criticism, comment, news reporting, teaching, scholarship, education and research.

Fair use is a use permitted by copyright statute that might otherwise be infringing.

## License
Read the License in LICENSE.

[Animal Crossing Music Extension]: https://chrome.google.com/webstore/detail/animal-crossing-music/fcedlaimpcfgpnfdgjbmmfibkklpioop
[Google Drive]: https://drive.google.com/file/d/1i4v-J-Ur_8hgYdgLhyupzzKWjBg_-cLv/view