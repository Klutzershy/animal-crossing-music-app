package acmusicapp;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainWindowController implements Initializable {
		
	@FXML
	private BorderPane mainPane;
	@FXML
	protected Label timeLabel;
	@FXML
	private Label meridiemLabel;
	@FXML
	private Label dateLabel;
	@FXML
	private Label weekDayLabel;
	@FXML
	private Slider volumeSlider;
	
	@FXML
	private RadioButton gcChoice;
	@FXML
	private RadioButton cfChoice;
	@FXML
	private RadioButton nlChoice;
	
	private Timer timer;
	
	private boolean optionsInvisible = false;
	
	private Stage primaryStage = AnimalCrossingMusicApp.getStage();
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		setXandY();
		setDragAndDrop(primaryStage);
		AudioPlayer.getInstance().playHour();
		volumeSlider.setValue(StateManager.getInstance().getVolumeValue());
		setHandleVolume();
		setRadioSelection();
		updateLabels();
		checkLabelsNeedUpdate();
		StateManager.getInstance().setVolumeSlider(volumeSlider);
		handleGameChoice();
		optionsVisible();
		primaryStage.setOnCloseRequest(event -> {
			Platform.exit();
			System.exit(0);
		});
	}
	
	private void setXandY() {
		double xValue = StateManager.getInstance().getxValue();
		double yValue = StateManager.getInstance().getyValue();
		if(xValue != -1 && yValue != -1) {
			primaryStage.setX(xValue);
			primaryStage.setY(yValue);
		}
		
	}

	//Updates the Labels
	private void updateLabels() {
		timeLabel.setText(TimeKeeper.getInstance().getTime());
		meridiemLabel.setText(updateMeridiem());
		dateLabel.setText(TimeKeeper.getInstance().getDate());
		weekDayLabel.setText(TimeKeeper.getInstance().getWeekDay());
	}
	
	//Runs timer which checks for the time and updates the labels every second
	private void checkLabelsNeedUpdate() {
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						updateLabels();
					}
				});
			}
			
		}, 0, 1000);
		
	}
	
	private String updateMeridiem() {
		int currentHour = TimeKeeper.getInstance().getHour();
		if(currentHour > 11 && currentHour < 24) {
			return "PM";
		} else if(currentHour < 12 && currentHour > -1) {
			return "AM";
		}
		return "Invalid";
	}
	
	public Timer getTimer() {
		return timer;
	}
	
	double xOffset = 0.0;
	double yOffset = 0.0;
	
	private void setDragAndDrop(Stage stage) {
		mainPane.setOnMousePressed(event -> {
			xOffset = stage.getX() - event.getScreenX();
			yOffset = stage.getY() - event.getScreenY();
		});
		
		mainPane.setOnMouseDragged(event -> {
			stage.setX(event.getScreenX() + xOffset);
			stage.setY(event.getScreenY() + yOffset);
			StateManager.getInstance().setxValue(stage.getX());
			StateManager.getInstance().setyValue(stage.getY());
		});
	}
	
	@FXML
	private void optionsVisible() {
		if(optionsInvisible) {
			volumeSlider.setVisible(true);
			gcChoice.setVisible(true);
			cfChoice.setVisible(true);
			nlChoice.setVisible(true);
			optionsInvisible = false;
		} else {
			volumeSlider.setVisible(false);
			gcChoice.setVisible(false);
			cfChoice.setVisible(false);
			nlChoice.setVisible(false);
			optionsInvisible = true;
		}
	}
	
	private void setHandleVolume() {
		volumeSlider.setShowTickMarks(false);
		volumeSlider.setMax(1);
		volumeSlider.valueProperty().addListener((observable, oldvalue, newvalue) -> {
			double volume = newvalue.doubleValue();
			AudioPlayer.getInstance().setVolume(volume);
		});
		AudioPlayer.getInstance().setVolume(volumeSlider.getValue());
	}
	
	private void setRadioSelection() {
		switch(StateManager.getInstance().getGame()) {
		case "game-cube":
			gcChoice.setSelected(true);
			break;
		case "city-folk":
			cfChoice.setSelected(true);
			break;
		case "new-leaf":
			nlChoice.setSelected(true);
			break;
		}
	}
	
	private void handleGameChoice() {
		gcChoice.selectedProperty().addListener((observable, notselected, isselected) -> {
			if(isselected) {
				StateManager.getInstance().setGame("game-cube");
				AudioPlayer.getInstance().switchMedia();
				AudioPlayer.getInstance().setVolume(volumeSlider.getValue());
			}
		});
		cfChoice.selectedProperty().addListener((observable, notselected, isselected) -> {
			if(isselected) {
				StateManager.getInstance().setGame("city-folk");
				AudioPlayer.getInstance().switchMedia();
				AudioPlayer.getInstance().setVolume(volumeSlider.getValue());
			}
		});
		nlChoice.selectedProperty().addListener((observable, notselected, isselected) -> {
			if(isselected) {
				StateManager.getInstance().setGame("new-leaf");
				AudioPlayer.getInstance().switchMedia();
				AudioPlayer.getInstance().setVolume(volumeSlider.getValue());
			}
		});
	}

	@FXML
	private void handleExit() {
		StateManager.getInstance().saveState();
		System.exit(0);
	}

}
