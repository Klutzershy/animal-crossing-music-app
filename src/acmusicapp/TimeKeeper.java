package acmusicapp;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class TimeKeeper {
	
	private static TimeKeeper instance = new TimeKeeper();
	
	private String currentTime;
	
	private Timer timer;
	
	private Calendar calendar = new GregorianCalendar();
	
	private int nextHour = 3600 - (getMinute() * 60);
	
	private int  playingHour = LocalTime.now().getHour();
	
	public TimeKeeper() {
		
		SimpleDateFormat df = new SimpleDateFormat("hh:mm");
		df.setTimeZone(TimeZone.getDefault());
		currentTime = df.format(calendar.getTime());
		
		checkTime();
	}
	
	public static TimeKeeper getInstance() {
		return instance;
	}
	
	
	public String getTime() {
		updateTime();
		return currentTime;
	}
	
	public int getPlayingHour() {
		return playingHour;
	}
	
	public int getHour() {
		return LocalTime.now().getHour();
	}
	
	public int getMinute() {
		return LocalTime.now().getMinute();
	}
	
	public String getDate() {
		SimpleDateFormat df = new SimpleDateFormat("M/dd");
		return df.format(calendar.getTime());
	}
	
	public String getWeekDay() {
		SimpleDateFormat df = new SimpleDateFormat("E");
		return df.format(calendar.getTime());
	}
	
	//Checks time every second
	public void checkTime() {
		int second = 1000;
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				int currentHour = TimeKeeper.getInstance().getHour();
				if(currentHour != playingHour) {
					AudioPlayer.getInstance().playHour();
					playingHour = currentHour;
				}
				if(nextHour == 0) {
					nextHour = 3600;
				}
				--nextHour;
			}
			
		}, 0, second);
	}
	
	//Sets the current time to the current system time.
	public void updateTime() {
		ZonedDateTime updateTime = ZonedDateTime.now(TimeZone.getDefault().toZoneId());
		currentTime = DateTimeFormatter.ofPattern("hh:mm").format(updateTime);
	}
	
	public Timer getTimer() {
		return timer;
	}
	
	
}
