package acmusicapp;
import java.util.Timer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AnimalCrossingMusicApp extends Application {

	private static Stage primaryStage;
	
	@Override
	public void start(Stage stage) throws Exception {
		AnimalCrossingMusicApp.primaryStage = stage;
		Parent root = FXMLLoader.load(getClass().getResource("main_window.fxml"));
		Scene scene = new Scene(root);
		scene.getStylesheets().add("styles.css");
		stage.getIcons().add(new Image(AnimalCrossingMusicApp.class.getResourceAsStream("leaf.png")));
		stage.setTitle("Animal Crossing Clock");
		stage.initStyle(StageStyle.UNDECORATED);
		stage.setScene(scene);
		stage.show();
	}
	
	@Override
	public void stop() {
		Timer tkTimer = TimeKeeper.getInstance().getTimer();
		tkTimer.cancel();
		AudioPlayer.getInstance().endMedPlayer();
		primaryStage.close();
	}
	
	@Override
	public void init() {
		StateManager.getInstance().loadState();
	}
	
	public static Stage getStage() {
		return primaryStage;
	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
