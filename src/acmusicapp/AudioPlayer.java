package acmusicapp;

import java.net.URL;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class AudioPlayer {
	
	private static AudioPlayer instance = new AudioPlayer();
	
	private boolean isPlaying = false; 
	
	private MediaPlayer medPlayer;
	
	
	String gameChoice = StateManager.getInstance().getGame();
	private Media musicMedia;
	
	public static AudioPlayer getInstance() {
		return instance;
	}
	
	protected void playHour() {
		
		if(!isPlaying) {
			
			updateHour();
			medPlayer = new MediaPlayer(musicMedia);
			medPlayer.setCycleCount(MediaPlayer.INDEFINITE);
			medPlayer.play();
			isPlaying = true;
			
		} else if(isPlaying) {
			updateHour();
			medPlayer.stop();
			this.medPlayer = new MediaPlayer(musicMedia);
			medPlayer.setCycleCount(MediaPlayer.INDEFINITE);
			medPlayer.play();
			isPlaying =true;
		}
		
	}
	
	public void switchMedia() {
		gameChoice = StateManager.getInstance().getGame();
		playHour();
	}
	
	public void endMedPlayer() {
		medPlayer.stop();
		medPlayer.dispose();
	}
	
	public void updateHour() {
		Integer currentHour = TimeKeeper.getInstance().getHour();
		URL uri = getClass().getResource("/music/" + gameChoice + "/" + currentHour.toString() + ".mp3");
		musicMedia = new Media(uri.toString());
	}
	
	public void setVolume(double volumeLevel) {
		medPlayer.setVolume(volumeLevel);
	}
}
