package acmusicapp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;

import javafx.scene.control.Slider;

public class StateManager {
	//Options, manages the state of the music choice and all
	private static StateManager instance = new StateManager();

	private Slider volumeSlider = new Slider();
	
	private String gameChoice = "game-cube";
	
	private double volumeValue = 1.0;
	
	private double xValue = -1;
	private double yValue = -1;
	
	public static StateManager getInstance() {
		return instance;
	}

	public void setVolumeSlider(Slider volumeSlider) {
		this.volumeSlider = volumeSlider;
	}
	
	public Slider getVolumeSlider() {
		return volumeSlider;
	}
	
	public double getVolumeValue() {
		return volumeValue;
	}

	public double getxValue() {
		return xValue;
	}

	public void setxValue(double xValue) {
		this.xValue = xValue;
	}

	public double getyValue() {
		return yValue;
	}

	public void setyValue(double yValue) {
		this.yValue = yValue;
	}

	public String getGame() {
//		if(!(gameChoice.equals("null")) && gameChoice != null ) {
		return gameChoice;		
	}
	
	public void setGame(String gameChoice) {
		this.gameChoice = gameChoice;
	}
	//TODO: Config ini makes the program not close
	public void saveState() {
		File configFile = new File("./config.ini");
//		BufferedWriter writer = null;
		try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(configFile)))) {
			writer.write("volume=" + volumeSlider.getValue());
			writer.newLine();
			writer.write("game=" + gameChoice);
			writer.newLine();
			writer.write("X=" + xValue);
			writer.newLine();
			writer.write("Y=" + yValue);
			writer.flush();
			writer.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void loadState() {
		File configFile = new File("./config.ini");
		if(configFile.exists()) {
		BufferedReader reader = null;
		HashMap<String, String> map = new HashMap<String, String>();
		try {
			reader = new BufferedReader(new FileReader(configFile));
			String line = null;
			String[] configOption = null;
			while((line = reader.readLine()) != null) {
				configOption = line.split("=");
				map.put(configOption[0], configOption[1]);
			}
			
			gameChoice = map.get("game");
			if(map.get("volume") != null) {
				Double volumeValue = Double.parseDouble(map.get("volume"));
				this.volumeValue = volumeValue;
			}
			
			if(map.get("X") != null) {
				xValue = Double.parseDouble(map.get("X"));
			}
			
			if(map.get("Y") != null) {
				yValue = Double.parseDouble(map.get("Y"));
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		}
	}

}
